import { Controller, ControllerAction } from "../../../application/ports/controllers/controller.port";
import { ResponseHandler, ResponseModel } from "../../../application/ports/responses/response.port";
import { RequestModel } from "../../../application/ports/requests/request.port";
import { Purchase, PurchaseCreateInput } from "../../../entities/models/purchase/purchase.model.entity";
import { PurchaseUseCase } from "../../../application/use-cases/purchase/purchase.use-case";

export type PurchaseCreateRequestInput = Partial<PurchaseCreateInput>;

export type PurchaseParamsRequestInput = {
    id: number;
}

export class PurchaseController implements Controller {
    public static instance: PurchaseController;
    [name: string]: ControllerAction | unknown;

    constructor(private readonly purchaseUseCase: PurchaseUseCase, private readonly responseHandler: ResponseHandler) {
        PurchaseController.instance = this;
    }

    /**
     * Create a new purchase
     * @param request 
     * @returns {Promise<ResponseModel<Purchase>>}
     */
    async create(request: RequestModel<PurchaseCreateRequestInput>): Promise<ResponseModel<Purchase>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error('Body was not provided');
            }
            const input = PurchaseController.instance.validateCreatebody(request.body);
            const purchase = await PurchaseController.instance.purchaseUseCase.create(input);
            return await PurchaseController.instance.responseHandler.response('success', 201, purchase);
        } catch (error) {
            return await PurchaseController.instance.responseHandler.response('error', 404, error.message);
        }
    }

    /**
 * Delete an purchases based on its id
 * @param request 
 * @returns {Promise<ResponseModel<Purchase>>}
 */
    async delete(request: RequestModel<PurchaseParamsRequestInput>): Promise<ResponseModel<Purchase>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }
            const purchase = await PurchaseController.instance.purchaseUseCase.getById(request.params.id);
            if (!purchase) {
                const error: any = new Error('Purchase not found');
                return await PurchaseController.instance.responseHandler.response('error', 404, error.message);
            }
            await PurchaseController.instance.purchaseUseCase.delete(request.params.id);
            return await PurchaseController.instance.responseHandler.response('success', 204, purchase);
        } catch (err: any) {
            return await PurchaseController.instance.responseHandler.response('error', 400, err.message);
        }
    }

    /**
     * Get an Purchases by it id
     * @param request 
     * @returns {Promise<ResponseModel<Purchase>>}
     */
    async getById(request: RequestModel<PurchaseParamsRequestInput>): Promise<ResponseModel<Purchase>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }
            const purchase = await PurchaseController.instance.purchaseUseCase.getById(request.params.id);
            if (!purchase) {
                const error: any = new Error('Purchase not found')
                return await PurchaseController.instance.responseHandler.response('error', 404, error.message);
            }
            return await PurchaseController.instance.responseHandler.response('success', 200, purchase);
        } catch (err: any) {
            return await PurchaseController.instance.responseHandler.response('error', 400, err.message);
        }
    }

    /**
     * Get all purchases
     * @returns {Promise<ResponseModel<Purchase[]>>}
     */
    async getAll(): Promise<ResponseModel<Purchase[]>> {
        try {
            const purchases = await PurchaseController.instance.purchaseUseCase.getAll();
            return await PurchaseController.instance.responseHandler.response('success', 200, purchases);
        } catch (error) {
            return PurchaseController.instance.responseHandler.response('success', 400, error.messsage);
        }
    }

    /**
     * Validate request params
     * @param createInput 
     */
    validateCreatebody(createInput: PurchaseCreateRequestInput): PurchaseCreateInput {
        const { userId, itemsIds } = createInput;
        if (userId === undefined || itemsIds === undefined) {
            throw new Error('Some fields were not provided');
        }
        return {
            userId: userId,
            itemsIds: itemsIds
        }
    }


}