import { Controller, ControllerAction } from "../../../application/ports/controllers/controller.port";
import { ResponseHandler, ResponseModel } from "../../../application/ports/responses/response.port";
import { RequestModel } from "../../../application/ports/requests/request.port";
import { Item, ItemCreateInput, ItemUpdateInput } from "../../../entities/models/item/item.model.entity";
import { ItemUseCase } from "../../../application/use-cases/item/item.use-case";

export type ItemCreateRequestInput = Partial<ItemCreateInput>;

export type ItemUpdateRequestInput = Partial<ItemUpdateInput>;

export type ItemParamsRequestInput = {
    id: number;
};


export class ItemController implements Controller {
    public static instance: ItemController;
    [name: string]: ControllerAction | unknown;

    constructor(private readonly itemUseCase: ItemUseCase, private readonly responseHandler: ResponseHandler) {
        ItemController.instance = this;
    }

    /**
     * Create a new item
     * @param request 
     * @returns {Promise<ResponseModel<Item>>}
     */
    async create(request: RequestModel<ItemCreateRequestInput>): Promise<ResponseModel<Item>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error('Body was not provided');
            }
            const input = ItemController.instance.validateCreatebody(request.body);
            const item = await ItemController.instance.itemUseCase.create(input);
            return await ItemController.instance.responseHandler.response('success', 201, item);
        } catch (error) {
            return await ItemController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    /**
     * Update item attributes
     * @param request 
     * @returns {Promise<ResponseModel<Item | undefined>>}
     */
    async update(request: RequestModel<ItemUpdateRequestInput, ItemParamsRequestInput>): Promise<ResponseModel<Item | undefined>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }

            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error('Body was not provided');
            }
            const item = await ItemController.instance.itemUseCase.getById(request.params.id);
            if (!item) {
                const error: any = new Error('Item not found');
                return await ItemController.instance.responseHandler.response('error', 404, error.message);
            }
            const input = ItemController.instance.validateUpdatebody(request.body);
            const updateItem = await ItemController.instance.itemUseCase.update(request.params.id, input);
            return await ItemController.instance.responseHandler.response('success', 200, updateItem);
        } catch (err: any) {
            return await ItemController.instance.responseHandler.response('error', 500, err.message);
        }
    }

    /**
     * Delete an item based on its id
     * @param params 
     * @returns {Promise<ResponseModel<Item>>}
     */
    async delete(params: RequestModel<ItemParamsRequestInput>): Promise<ResponseModel<Item>> {
        try {
            if (!params.params?.id) {
                throw new Error('Could not get id');
            }
            const item = await ItemController.instance.itemUseCase.getById(params.params.id);
            if (!item) {
                const error: any = new Error('Item not found');
                return await ItemController.instance.responseHandler.response('error', 404, error.message);
            }
            await ItemController.instance.itemUseCase.delete(params.params.id);
            return await ItemController.instance.responseHandler.response('success', 204, item);
        } catch (err: any) {
            return await ItemController.instance.responseHandler.response('error', 500, err.message);
        }
    }

    /**
  * Get an Item by it id
  * @param request 
  * @returns {Promise<ResponseModel<Item>>}
  */
    async getById(request: RequestModel<ItemParamsRequestInput>): Promise<ResponseModel<Item>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }
            const item = await ItemController.instance.itemUseCase.getById(request.params.id);
            if (!item) {
                const error: any = new Error('Item not found')
                return await ItemController.instance.responseHandler.response('error', 404, error.message);
            }
            return await ItemController.instance.responseHandler.response('success', 200, item);
        } catch (err: any) {
            return await ItemController.instance.responseHandler.response('error', 500, err.message);
        }
    }

    /**
     * Get all Items
     * @returns {Promise<ResponseModel<Item[]>>}
     */
    async getAll(): Promise<ResponseModel<Item[]>> {
        try {
            const items = await ItemController.instance.itemUseCase.getAll();
            return await ItemController.instance.responseHandler.response('success', 200, items);
        } catch (error) {
            return ItemController.instance.responseHandler.response('success', 500, error.messsage);
        }
    }

    /**
     * Validate request params
     * @param createInput 
     */
    validateCreatebody(createInput: ItemCreateRequestInput): ItemCreateInput {
        const { title, description, price, quantity, creatorUserId } = createInput;
        if (title === undefined || description === undefined || price === undefined || quantity === undefined || creatorUserId === undefined) {
            throw new Error('Some fields were not provided');
        }
        return {
            title: title,
            description: description,
            price: price,
            quantity: quantity,
            creatorUserId: creatorUserId
        }
    }

    /**
         * Validate request params
         * @param createInput 
         */
    validateUpdatebody(updateInput: ItemUpdateRequestInput): ItemUpdateInput {
        const { title, description, price, quantity, creatorUserId } = updateInput;
        let updateOutput: ItemUpdateInput = {};
        if (title !== undefined) {
            updateOutput.title = title;
        }
        if (description !== undefined) {
            updateOutput.description = description;
        }
        if (price !== undefined) {
            updateOutput.price = price;
        }
        if (quantity !== undefined) {
            updateOutput.quantity = quantity;
        }
        if (creatorUserId !== undefined) {
            updateOutput.creatorUserId = creatorUserId;
        }
        return updateOutput;
    }

}