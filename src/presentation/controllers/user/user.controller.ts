import { Controller, ControllerAction } from "../../../application/ports/controllers/controller.port";
import { ResponseHandler, ResponseModel } from "../../../application/ports/responses/response.port";
import { RequestModel } from "../../../application/ports/requests/request.port";
import { User, UserCreateInput, UserUpdateInput } from "../../../entities/models/user/user.model.entity";
import { UserUseCase } from "../../../application/use-cases/user/user.use-case";

export type UserCreateRequestInput = Partial<UserCreateInput>;

export type UserUpdateRequestInput = Partial<UserUpdateInput>;

export type UserParamsRequestInput = {
    id: number;
}

export class UserController implements Controller {
    public static instance: UserController;
    [name: string]: ControllerAction | unknown;

    constructor(private readonly userUseCase: UserUseCase, private readonly responseHandler: ResponseHandler) {
        UserController.instance = this;
    }

    /**
     * Create a new user
     * @param request 
     * @returns {Promise<ResponseModel<User>>}
     */
    async create(request: RequestModel<UserCreateRequestInput>): Promise<ResponseModel<User>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error('Body was not provided');
            }
            const input = UserController.instance.validateCreatebody(request.body);
            const user = await UserController.instance.userUseCase.create(input);
            return await UserController.instance.responseHandler.response('success', 201, user);
        } catch (error) {
            if (error.message === 'Some fields were not provided' || error.message === 'Body was not provided') {
                return await UserController.instance.responseHandler.response('error', 400, error.message);
            }
            return await UserController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    /**
     * Update users attributes
     * @param request 
     * @returns {Promise<ResponseModel<User | undefined>>}
     */
    async update(request: RequestModel<UserUpdateRequestInput, UserParamsRequestInput>): Promise<ResponseModel<User | undefined>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }

            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error('Body was not provided');
            }
            const user = await UserController.instance.userUseCase.getById(request.params.id);
            if (!user) {
                const error: any = new Error('user not found');
                return await UserController.instance.responseHandler.response('error', 404, error.message);
            }
            const input = UserController.instance.validateUpdatebody(request.body);
            const updateUser = await UserController.instance.userUseCase.update(request.params.id, input);
            return await UserController.instance.responseHandler.response('success', 200, updateUser);
        } catch (err: any) {
            return await UserController.instance.responseHandler.response('error', 500, err.message);
        }
    }

    /**
     * Delete an users based on its id
     * @param request 
     * @returns {Promise<ResponseModel<User>>}
     */
    async delete(request: RequestModel<UserParamsRequestInput>): Promise<ResponseModel<User>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }
            const user = await UserController.instance.userUseCase.getById(request.params.id);
            if (!user) {
                const error: any = new Error('user not found');
                return await UserController.instance.responseHandler.response('error', 404, error.message);
            }
            await UserController.instance.userUseCase.delete(request.params.id);
            return await UserController.instance.responseHandler.response('success', 204, user);
        } catch (err: any) {
            return await UserController.instance.responseHandler.response('error', 500, err.message);
        }
    }

    /**
    * Get an users by it id
    * @param request 
    * @returns {Promise<ResponseModel<User>>}
    */
    async getById(request: RequestModel<UserParamsRequestInput>): Promise<ResponseModel<User>> {
        try {
            if (!request.params?.id) {
                throw new Error('Could not get id');
            }
            const user = await UserController.instance.userUseCase.getById(request.params.id);
            if (!user) {
                const error: any = new Error('user not found')
                return await UserController.instance.responseHandler.response('error', 404, error.message);
            }
            return await UserController.instance.responseHandler.response('success', 200, user);
        } catch (err: any) {
            return await UserController.instance.responseHandler.response('error', 500, err.message);
        }
    }

    /**
     * Get all users
     * @returns {Promise<ResponseModel<User[]>>}
     */
    async getAll(): Promise<ResponseModel<User[]>> {
        try {
            const users = await UserController.instance.userUseCase.getAll();
            return await UserController.instance.responseHandler.response('success', 200, users);
        } catch (error) {
            return UserController.instance.responseHandler.response('success', 500, error.messsage);
        }
    }

    /**
     * Validate request params
     * @param createInput 
     */
    validateCreatebody(createInput: UserCreateRequestInput): UserCreateInput {
        const { firstName, secondName, lastName, secondLastName, email, budget } = createInput;
        if (firstName === undefined || secondName === undefined || lastName === undefined || secondLastName === undefined || email === undefined || budget === undefined) {
            throw new Error('Some fields were not provided');
        }
        return {
            budget: budget,
            email: email,
            firstName: firstName,
            lastName: lastName,
            secondLastName: secondLastName,
            secondName: secondName
        }
    }

    /**
     * Validate request params
     * @param createInput 
     */
    validateUpdatebody(updateInput: UserUpdateRequestInput): UserUpdateInput {
        const { firstName, secondName, lastName, secondLastName, email, budget } = updateInput;
        const updateOutput: UserUpdateInput = {}
        if (firstName !== undefined) {
            updateOutput.firstName = firstName
        }
        if (secondName !== undefined) {
            updateOutput.secondName = secondName
        }
        if (lastName !== undefined) {
            updateOutput.lastName = lastName
        }
        if (secondLastName !== undefined) {
            updateOutput.secondLastName = secondLastName
        }
        if (email !== undefined) {
            updateOutput.email = email
        }
        if (budget !== undefined) {
            updateOutput.budget = budget
        }
        return updateOutput;
    }

}