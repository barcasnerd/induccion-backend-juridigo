import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, DeleteDateColumn } from "typeorm";
import { PurchaseEntity } from "./purchase.entity";

@Entity()
export class ItemEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ type: 'varchar', unique: true, length: 100 })
    title!: string;

    @Column({ type: 'varchar', length: 100 })
    description!: string;

    @Column({ nullable: false })
    price!: number;

    @Column({ nullable: false })
    quantity!: number;

    @CreateDateColumn({ type: 'timestamptz' })
    createdDate!: Date;

    @UpdateDateColumn({ type: 'timestamptz' })
    updatedDate!: Date;

    @DeleteDateColumn({ type: 'timestamptz' })
    deletedDate!: Date;

    @Column({ nullable: false })
    creatorUserId!: number;

    @ManyToOne(() => PurchaseEntity, purchase => purchase.items)
    purchase!: PurchaseEntity;

    constructor(title: string, description: string, price: number, quantity: number, creatorUserId: number) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.creatorUserId = creatorUserId;
    }

}