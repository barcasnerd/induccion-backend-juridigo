import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, DeleteDateColumn } from "typeorm";
import { UserEntity } from "./user.entity";
import { ItemEntity } from "./item.entity";

@Entity()
export class PurchaseEntity {
    [x: string]: any;

    @PrimaryGeneratedColumn()
    id!: number;

    @CreateDateColumn({ type: 'timestamptz' })
    purchaseDate!: Date;

    @CreateDateColumn({ type: "timestamptz" })
    createdDate!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedDate!: Date;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedDate!: Date;

    @Column({ nullable: false })
    userId!: number;

    @OneToMany(() => ItemEntity, item => item.purchase)
    items!: ItemEntity[];



    constructor(userId: number, items: ItemEntity[]) {
        this.userId = userId;
        this.items = items
    }
}