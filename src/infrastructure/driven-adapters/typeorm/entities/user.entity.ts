import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from "typeorm";

@Entity()
export class UserEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ type: 'varchar', length: 100, nullable: false })
    firstName!: string;

    @Column({ type: 'varchar', length: 100 })
    secondName!: string;

    @Column({ type: 'varchar' })
    lastName!: string;

    @Column({ type: 'varchar', length: 100 })
    secondLastName!: string;

    @Column({ type: 'varchar', length: 100, nullable: false, unique: true })
    email!: string;

    @Column({ default: 0 })
    budget!: number;

    @CreateDateColumn({ type: "timestamptz" })
    createdDate!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedDate!: Date;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedDate!: Date;

    constructor(firstName: string, email: string) {
        this.firstName = firstName;
        this.email = email;
    }

}