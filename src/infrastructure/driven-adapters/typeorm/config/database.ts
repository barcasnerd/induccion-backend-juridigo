const dotenv = require('dotenv-yaml');
dotenv.config();
import { ConnectionOptions } from "typeorm";

console.log('Environment:', process.env.NODE_ENV);

const connectionOptions: ConnectionOptions = {
    type: "postgres",
    "host": process.env.TYPEORM_HOST!,
    "port": parseInt(process.env.TYPEORM_PORT + ""),
    "username": process.env.TYPEORM_USERNAME,
    "password": process.env.TYPEORM_PASSWORD,
    "database": process.env.TYPEORM_DATABASE_TEST,
    logging: ["error"],
    synchronize: true,
    entities: [
        __dirname + "/../entities/**",
    ],
    migrations: [
        __dirname + "/../migrations/**"
    ],
    cli: {
        entitiesDir: "src/models",
        migrationsDir: "src/migrations"
    },
    name: "default"
};

export = connectionOptions;
