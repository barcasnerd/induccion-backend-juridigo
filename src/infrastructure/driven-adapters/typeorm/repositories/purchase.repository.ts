import { IPurchaseRepository } from "../../../../application/ports/repositories/purchase/purchase.repository";
import { PurchaseCreateInput, Purchase } from "../../../../entities/models/purchase/purchase.model.entity";
import { UserCreateInput } from "../../../../entities/models/user/user.model.entity";
import { PurchaseEntity } from "../entities/purchase.entity";
import { Connection, Repository, EntityManager, QueryRunner } from "typeorm";
import { itemEntityToDomainItem, purchaseEntityToDomainPurchase } from "./commmon/helpers/transform-data-to-domain";
import { UserEntity } from "../entities/user.entity";
import { ItemEntity } from "../entities/item.entity";
import { IPurchaseUseCase } from "../../../../entities/use-cases/purchase/purchase.use-case.entity";

export class PurchaseRepository implements IPurchaseRepository {
    private repository: Repository<PurchaseEntity>;
    private itemRepository: Repository<ItemEntity>;
    private userRepository: Repository<UserEntity>;
    private manager: EntityManager;
    private queryRunner: QueryRunner;
    constructor(connection: Connection) {
        this.repository = connection.getRepository(PurchaseEntity);
        this.itemRepository = connection.getRepository(ItemEntity);
        this.userRepository = connection.getRepository(UserEntity);
        this.manager = connection.manager;
        this.queryRunner = connection.createQueryRunner();
    }

    /**
     * Creates a purchase
     */
    async create(purchase: PurchaseCreateInput): Promise<Purchase> {
        // find if the user exist
        let user = await this.userRepository.findOne(purchase.userId);
        if (!user) {
            throw new Error('User for the purchase not found');
        }

        // Find items by id in the repository 
        let items = await this.manager.createQueryBuilder(ItemEntity, "item")
            .where("item.id IN (:...items)", { items: purchase.itemsIds })
            .andWhere("item.quantity > :zero", { zero: 0 })
            .getMany();
        if (items.length !== purchase.itemsIds.length) {
            throw new Error('Some items do not exist');
        }

        // calculates the total purchase
        const total = items.map(item => item.price).reduce((a, b) => a + b, 0);
        if (user.budget < total) {
            throw new Error('Could not buy, low user budget');
        }

        // reduce user budget and items quantity
        user.budget = user.budget - total;
        items = items.map((item) => {
            item.quantity = item.quantity - 1;
            return item
        });

        // creates the entity
        const draftPurchase = new PurchaseEntity(user.id, items);


        // save changes if nothing fails
        await this.queryRunner.startTransaction();
        try {
            await this.queryRunner.manager.save(user);
            items.forEach(async (item) => await this.queryRunner.manager.save(item));
            await this.queryRunner.manager.save(draftPurchase);

            await this.queryRunner.commitTransaction();
        } catch (error) {
            await this.queryRunner.rollbackTransaction();
            throw new Error('Somthing went wrong, could not create purchase');
        }

        return purchaseEntityToDomainPurchase(draftPurchase);

    }

    /**
     * Deletes a purchase by its id
     */
    async delete(id: number): Promise<Purchase | undefined> {
        let purchase = await this.repository.findOne(id)
        if (!purchase) {
            return undefined;
        }
        purchase = await this.repository.softRemove(purchase);
        return purchaseEntityToDomainPurchase(purchase)
    }

    /**
     * Gets purchase information by its id
     */
    async getById(id: number): Promise<Purchase | undefined> {
        let purchase = await this.repository.findOne({ relations: ["items"], where: { id: id } });
        if (!purchase) {
            return undefined;
        }

        return purchaseEntityToDomainPurchase(purchase);
    }

    /**
     * Get all purchases saved
     */
    async getAll(): Promise<Purchase[]> {
        const purchases = await this.repository.find({ relations: ["items"] });
        return purchases.map(purchase => purchaseEntityToDomainPurchase(purchase));

    }





}

