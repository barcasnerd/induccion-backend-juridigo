import { ItemEntity } from "../../../entities/item.entity";
import { PurchaseEntity } from "../../../entities/purchase.entity";
import { UserEntity } from "../../../entities/user.entity";
import { Item } from "../../../../../../entities/models/item/item.model.entity";
import { User } from "../../../../../../entities/models/user/user.model.entity";
import { Purchase } from "../../../../../../entities/models/purchase/purchase.model.entity";

export function itemEntityToDomainItem(itemEntity: ItemEntity): Item {
    return {
        id: itemEntity.id,
        title: itemEntity.title,
        description: itemEntity.description,
        price: itemEntity.price,
        quantity: itemEntity.quantity,
        createdDate: itemEntity.createdDate,
        updatedDate: itemEntity.updatedDate,
        creatorUserId: itemEntity.creatorUserId
    }
}

export function userEntityToDomainUser(userEntity: UserEntity): User {
    return {
        id: userEntity.id,
        createdDate: userEntity.createdDate,
        updatedDate: userEntity.updatedDate,
        firstName: userEntity.firstName,
        secondName: userEntity.secondName,
        lastName: userEntity.lastName,
        secondLastName: userEntity.secondLastName,
        email: userEntity.email,
        budget: userEntity.budget
    }
}

export function purchaseEntityToDomainPurchase(purchaseEntity: PurchaseEntity): Purchase {
    const { items } = purchaseEntity
    let itemIds: number[] = [];
    items.forEach((item) => {
        itemIds.push(item.id);
    });
    return {
        id: purchaseEntity.id,
        purchaseDate: purchaseEntity.purchaseDate,
        createdDate: purchaseEntity.createdDate,
        updatedDate: purchaseEntity.updatedDate,
        userId: purchaseEntity.userId,
        itemsIds: itemIds
    }
}