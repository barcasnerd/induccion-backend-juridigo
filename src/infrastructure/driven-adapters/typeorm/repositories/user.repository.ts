import { IUserRepository } from "../../../../application/ports/repositories/user/user.repository";
import { UserCreateInput, User, UserUpdateInput } from "../../../../entities/models/user/user.model.entity";
import { UserEntity } from "../entities/user.entity";
import { Connection, Repository } from "typeorm";
import { userEntityToDomainUser } from "./commmon/helpers/transform-data-to-domain";

export class UserRepository implements IUserRepository {
    private repository: Repository<UserEntity>;
    constructor(connection: Connection) {
        this.repository = connection.getRepository(UserEntity);
    }

    /**
     * Creates an user
     * @param user 
     * @returns 
     */
    async create(user: UserCreateInput): Promise<User> {
        let draftUser = new UserEntity(user.firstName, user.email);
        const { secondName, secondLastName, lastName, budget } = user;
        if (secondName) {
            draftUser.secondName = secondName;
        }
        if (secondLastName) {
            draftUser.secondLastName = secondLastName;
        }
        if (lastName) {
            draftUser.lastName = lastName;
        }
        if (budget) {
            draftUser.budget = budget;
        }
        const userFound = await this.repository.findOne({ email: user.email });
        if (userFound) {
            throw new Error('User already exist');
        }
        await this.repository.save(draftUser);
        return userEntityToDomainUser(draftUser);
    }

    /**
     * Updates user params
     * @param id 
     * @param user 
     * @returns 
     */
    async update(id: number, user: UserUpdateInput): Promise<User | undefined> {
        let updateUser = await this.repository.findOne(id);
        const { firstName, secondName, lastName, secondLastName, budget, email } = user;
        const userFound = await this.repository.findOne({ email: email });
        if (userFound) {
            return undefined;
        }
        if (!updateUser) {
            return undefined;
        }
        if (firstName) {
            updateUser.firstName = firstName
        }
        if (secondName) {
            updateUser.secondName = secondName
        }
        if (lastName) {
            updateUser.lastName = lastName
        }
        if (secondLastName) {
            updateUser.secondLastName = secondLastName
        }
        if (budget) {
            updateUser.budget = budget
        }
        if (email) {
            updateUser.email = email
        }
        updateUser = await this.repository.save(updateUser);
        return userEntityToDomainUser(updateUser);
    }

    /**
     * Deletes an user by its id
     * @param id 
     * @returns 
     */
    async delete(id: number): Promise<User | undefined> {
        const user = await this.repository.findOne(id);
        if (!user) {
            return undefined;
        }
        await this.repository.softRemove(user);
        return userEntityToDomainUser(user);
    }

    /**
     * Gets an user information by it id
     * @param id 
     * @returns 
     */
    async getById(id: number): Promise<User | undefined> {
        const user = await this.repository.findOne(id);
        if (!user) {
            return undefined;
        }
        return userEntityToDomainUser(user);
    }

    /**
     * get all users information
     * @returns 
     */
    async getAll(): Promise<User[]> {
        let users = await this.repository.find();
        const userslist = users.map(user => userEntityToDomainUser(user));
        return userslist;
    }






}