import { IItemRepository } from "../../../../application/ports/repositories/item/item.repository";
import { ItemCreateInput, Item, ItemUpdateInput } from "../../../../entities/models/item/item.model.entity";
import { ItemEntity } from "../entities/item.entity";
import { Connection, Repository } from "typeorm";
import { itemEntityToDomainItem } from "./commmon/helpers/transform-data-to-domain";
import { UserEntity } from "../entities/user.entity";

export class ItemRepository implements IItemRepository {

    private repository: Repository<ItemEntity>;
    private userRepository: Repository<UserEntity>;
    constructor(connection: Connection) {
        this.repository = connection.getRepository(ItemEntity);
        this.userRepository = connection.getRepository(UserEntity);
    }

    /**
     * Find a an user, then creates an item
     * @param item 
     * @returns {Promise<Item | undefined>}
     */
    async create(item: ItemCreateInput): Promise<Item> {
        const draftItem = new ItemEntity(item.title, item.description, item.price, item.quantity, item.creatorUserId);
        const foundUser = await this.userRepository.findOne(item.creatorUserId);
        if (!foundUser) {
            throw new Error('User creator not found');
        }
        await this.repository.save(draftItem);
        return itemEntityToDomainItem(draftItem);
    }

    /**
     * Update an item infromation refering to its id
     * @param id 
     * @param item 
     * @returns {Promise<Item | undefined>}
     */
    async update(id: number, item: ItemUpdateInput): Promise<Item | undefined> {
        let updateItem = await this.repository.findOne(id);
        if (!updateItem) {
            return undefined;
        }
        if (item.creatorUserId) {
            const creator = await this.userRepository.findOne(item.creatorUserId);
            if (!creator) {
                return undefined;
            }
            updateItem.creatorUserId = creator.id;
        }
        if (item.title) {
            updateItem.title = item.title
        }
        if (item.description) {
            updateItem.description = item.description
        }
        if (item.price) {
            updateItem.price = item.price
        }
        if (item.quantity) {
            updateItem.quantity = item.quantity
        }
        updateItem = await this.repository.save(updateItem);
        return itemEntityToDomainItem(updateItem);
    }

    /**
     * Delete an item  if exist based on its id
     * @param id 
     * @returns {Promise<Item | undefined>}
     */
    async delete(id: number): Promise<Item | undefined> {
        const item = await this.repository.findOne(id);
        if (item) {
            await this.repository.softRemove(item);
            return itemEntityToDomainItem(item);
        }
        return undefined;
    }

    /**
     * Get  an item by it id
     * @param id 
     * @returns {Promise<Item | undefined>}
     */
    async getById(id: number): Promise<Item | undefined> {
        const item = await this.repository.findOne(id);
        if (item) {
            return itemEntityToDomainItem(item);
        }
        return undefined;
    }

    /**
     * Return all items saved
     * @returns {Promise<Item[]>}
     */
    async getAll(): Promise<Item[]> {
        const items = await this.repository.find();
        return items.map(item => itemEntityToDomainItem(item));
    }

}