import { Router } from "express";
import { routeAdapter } from "../adapters/router-adapter";
import { createPurchaseControllerFactory } from "../../../../main/factories/controllers/purchase.controller.factory";

const purchaseRouter = () => {
    const router = Router();

    const { purchaseController } = createPurchaseControllerFactory()
    router.route('/')
        .get(routeAdapter(purchaseController.getAll))
        .post(routeAdapter(purchaseController.create))
    router.route('/:id')
        .get(routeAdapter(purchaseController.getById))
        .delete(routeAdapter(purchaseController.delete))
    return router;
}

export {
    purchaseRouter
}
