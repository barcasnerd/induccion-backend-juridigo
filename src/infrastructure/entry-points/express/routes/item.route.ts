import { Router } from "express";
import { routeAdapter } from "../adapters/router-adapter";
import { createItemControllerFactory } from "../../../../main/factories/controllers/item.controller.factory";

const itemRouter = () => {
    const router = Router();

    const { itemController } = createItemControllerFactory()
    router.route('/')
        .get(routeAdapter(itemController.getAll))
        .post(routeAdapter(itemController.create))
    router.route('/:id')
        .get(routeAdapter(itemController.getById))
        .patch(routeAdapter(itemController.update))
        .delete(routeAdapter(itemController.delete))
    return router;
}

export {
    itemRouter
}
