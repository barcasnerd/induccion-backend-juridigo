import { Router } from "express";
import { routeAdapter } from "../adapters/router-adapter";
import { createUserControllerFactory } from "../../../../main/factories/controllers/user.controller.factory";

const userRouter = () => {
    const router = Router();

    const { userController } = createUserControllerFactory()
    router.route('/')
        .get(routeAdapter(userController.getAll))
        .post(routeAdapter(userController.create))
    router.route('/:id')
        .get(routeAdapter(userController.getById))
        .patch(routeAdapter(userController.update))
        .delete(routeAdapter(userController.delete))
    return router;
}

export {
    userRouter
}
