import { Router } from 'express';
import { userRouter } from './user.route';
import { itemRouter } from './item.route';
import { purchaseRouter } from "./purchase.route";

const routes = () => {
    const router = Router();
    router.use('/users', userRouter());
    router.use('/items', itemRouter());
    router.use('/purchases', purchaseRouter());
    return router;
};


export {
    routes
};
