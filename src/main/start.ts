const dotenv = require('dotenv-yaml');
import { connectToDb } from '../infrastructure/driven-adapters/typeorm/config/db.config';
import { startApp } from '../infrastructure/entry-points/express/setup/server';


const path = __dirname + '/../../.env.yaml';

dotenv.config({ path });

console.log('Environment:', process.env.NODE_ENV);

if (process.env.NODE_ENV !== 'production') {
    dotenv.config();
}

async function start() {
    await connectToDb();
    startApp();
}

start().then();
