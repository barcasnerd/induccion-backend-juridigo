import { User, UserCreateInput, UserUpdateInput } from "../../../entities/models/user/user.model.entity";
import { IUserUseCase } from "../../../entities/use-cases/user/user.use-case.entity";
import { IUserRepository } from "../../ports/repositories/user/user.repository";

export class UserUseCase implements IUserUseCase {
    constructor(private readonly userRepository: IUserRepository) { }

    create(userInput: UserCreateInput): Promise<User> {
        return this.userRepository.create({ ...userInput });
    }

    update(id: number, userInput: UserUpdateInput): Promise<User | undefined> {
        const userID = this.userRepository.getById(id);
        if (!userID) {
            throw new Error("User with given ID could not be found");
        }
        return this.userRepository.update(id, { ...userInput });
    }

    delete(id: number): Promise<User | undefined> {
        const userID = this.userRepository.getById(id);
        if (!userID) {
            throw new Error("User with given ID could not be found");
        }
        return this.userRepository.delete(id);
    }

    getById(id: number): Promise<User | undefined> {
        return this.userRepository.getById(id);
    }

    getAll(): Promise<User[]> {
        return this.userRepository.getAll();
    }

}