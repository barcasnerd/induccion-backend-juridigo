import { Item, ItemCreateInput, ItemUpdateInput } from "../../../entities/models/item/item.model.entity";
import { IItemUseCase } from "../../../entities/use-cases/item/item.use-case.entity";
import { IItemRepository } from "../../ports/repositories/item/item.repository";

export class ItemUseCase implements IItemUseCase {
    constructor(private readonly itemRepository: IItemRepository) { }

    create(itemInput: ItemCreateInput): Promise<Item> {
        return this.itemRepository.create({ ...itemInput });
    }

    update(id: number, itemInput: ItemUpdateInput): Promise<Item | undefined> {
        const itemId = this.itemRepository.getById(id);
        if (!itemId) {
            throw new Error("Item with given ID could not be found");
        }
        return this.itemRepository.update(id, { ...itemInput });
    }

    delete(id: number): Promise<Item | undefined> {
        const itemId = this.itemRepository.getById(id);
        if (!itemId) {
            throw new Error("Item with given ID could not be found");
        }
        return this.itemRepository.delete(id);
    }

    getById(id: number): Promise<Item | undefined> {
        const itemId = this.itemRepository.getById(id);
        if (!itemId) {
            throw new Error('Item with given ID could not be found');
        }
        return itemId;
    }

    getAll(): Promise<Item[]> {
        return this.itemRepository.getAll();
    }

}
