import { Purchase, PurchaseCreateInput } from "../../../entities/models/purchase/purchase.model.entity";
import { IPurchaseUseCase } from "../../../entities/use-cases/purchase/purchase.use-case.entity";
import { IPurchaseRepository } from "../../ports/repositories/purchase/purchase.repository";

export class PurchaseUseCase implements IPurchaseUseCase {

    constructor(private readonly purchaseRepository: IPurchaseRepository) { }

    create(purchaseInput: PurchaseCreateInput): Promise<Purchase> {
        return this.purchaseRepository.create({ ...purchaseInput });
    }

    delete(id: number): Promise<Purchase | undefined> {
        const purchaseId = this.purchaseRepository.delete(id);
        if (!purchaseId) {
            throw new Error("Purchase with given ID could not be found");
        }
        return this.purchaseRepository.delete(id);
    }

    getById(id: number): Promise<Purchase | undefined> {
        const purchaseId = this.purchaseRepository.getById(id);
        if (!purchaseId) {
            throw new Error("Purchase with given ID could not be found");
        }
        return purchaseId;

    }

    getAll(): Promise<Purchase[]> {
        return this.purchaseRepository.getAll();
    }

}
