export interface RequestModel<Body = any, Params = Body, Query = Body, Headers = any> {
    body?: Body;
    params?: Params;
    query?: Query;
    headers?: Headers;
}

export interface MiddlewareRequestModel extends RequestModel {
    method?: string;
}
