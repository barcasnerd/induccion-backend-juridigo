import { Item, ItemCreateInput, ItemUpdateInput } from "../../../../entities/models/item/item.model.entity";

export interface IItemRepository {
    create(itemInput: ItemCreateInput): Promise<Item>;
    update(id: number, itemInput: ItemUpdateInput): Promise<Item | undefined>;
    delete(id: number): Promise<Item | undefined>;
    getById(id: number): Promise<Item | undefined>;
    getAll(): Promise<Array<Item>>;
}