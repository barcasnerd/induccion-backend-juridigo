import { User, UserCreateInput, UserUpdateInput } from "../../../../entities/models/user/user.model.entity";

export interface IUserRepository {
    create(userInput: UserCreateInput): Promise<User>;
    update(id: number, userInput: UserUpdateInput): Promise<User | undefined>;
    delete(id: number): Promise<User | undefined>;
    getById(id: number): Promise<User | undefined>;
    getAll(): Promise<Array<User>>;
}