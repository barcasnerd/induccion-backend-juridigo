import { Purchase, PurchaseCreateInput } from "../../../../entities/models/purchase/purchase.model.entity";

export interface IPurchaseRepository {
    create(purchaseInput: PurchaseCreateInput): Promise<Purchase>;
    delete(id: number): Promise<Purchase | undefined>;
    getById(id: number): Promise<Purchase | undefined>;
    getAll(): Promise<Array<Purchase>>;
}