import { User } from "../user/user.model.entity";

export interface Item {
    id: number;
    title: string;
    description: string;
    price: number;
    quantity: number;
    createdDate: Date;
    updatedDate: Date;
    creatorUserId: number;
}

export type ItemCreateInput = {
    title: string;
    description: string;
    price: number;
    quantity: number;
    creatorUserId: number;
}

export type ItemUpdateInput = {
    title?: string;
    description?: string;
    price?: number;
    quantity?: number;
    creatorUserId?: number;
}