import { Item } from "../item/item.model.entity";


export interface Purchase {
    id: number;
    purchaseDate: Date;
    createdDate: Date;
    updatedDate: Date;
    userId: number;
    itemsIds: number[];
}

export type PurchaseCreateInput = {
    userId: number;
    itemsIds: number[];
}
