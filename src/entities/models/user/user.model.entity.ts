export interface User {
    id: number;
    firstName: string;
    secondName: string;
    lastName: string;
    secondLastName: string;
    email: string;
    budget: number;
    createdDate: Date;
    updatedDate: Date;
}

export type UserCreateInput = {
    firstName: string;
    secondName: string;
    lastName: string;
    secondLastName: string;
    email: string;
    budget: number;
}

export type UserUpdateInput = {
    firstName?: string;
    secondName?: string;
    lastName?: string;
    secondLastName?: string;
    email?: string;
    budget?: number;
}