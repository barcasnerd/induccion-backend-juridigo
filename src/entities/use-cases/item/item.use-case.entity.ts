import { Item, ItemCreateInput, ItemUpdateInput } from "../../models/item/item.model.entity";

export interface IItemUseCase {
    create(itemInput: ItemCreateInput): Promise<Item>;
    update(id: number, itemInput: ItemUpdateInput): Promise<Item | undefined>;
    delete(id: number): Promise<Item | undefined>;
    getById(id: number): Promise<Item | undefined>;
    getAll(): Promise<Array<Item>>;
}