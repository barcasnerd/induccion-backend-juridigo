import { User, UserCreateInput, UserUpdateInput } from "../../models/user/user.model.entity";

export interface IUserUseCase {
    create(userInput: UserCreateInput): Promise<User>;
    update(id: number, userInput: UserUpdateInput): Promise<User | undefined>;
    delete(id: number): Promise<User | undefined>;
    getById(id: number): Promise<User | undefined>;
    getAll(): Promise<Array<User>>;
}