import { Purchase, PurchaseCreateInput } from "../../models/purchase/purchase.model.entity";

export interface IPurchaseUseCase {
    create(purchaseInput: PurchaseCreateInput): Promise<Purchase>;
    delete(id: number): Promise<Purchase | undefined>;
    getById(id: number): Promise<Purchase | undefined>;
    getAll(): Promise<Array<Purchase>>;
}