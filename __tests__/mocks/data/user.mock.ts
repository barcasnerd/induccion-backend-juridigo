import { User, UserCreateInput, UserUpdateInput } from "../../../src/entities/models/user/user.model.entity";

export const mocksUser: User[] = [
    {
        id: 1,
        firstName: "NameUser1",
        secondName: "SeconNameUser1",
        lastName: "LastNameUser1",
        secondLastName: "SecondLastNameUser1",
        email: "user1@email2.com",
        budget: 999999,
        createdDate: new Date(),
        updatedDate: new Date()
    },
    {
        id: 2,
        firstName: "NameUser2",
        secondName: "SeconNameUser2",
        lastName: "LastNameUser2",
        secondLastName: "SecondLastNameUser2",
        email: "user2@email2.com",
        budget: 0,
        createdDate: new Date(),
        updatedDate: new Date()
    }
]

export const mockUser: User = {
    id: 1,
    firstName: "NameUser1",
    secondName: "SeconNameUser1",
    lastName: "LastNameUser1",
    secondLastName: "SecondLastNameUser1",
    email: "user1@email2.com",
    budget: 999999,
    createdDate: new Date(),
    updatedDate: new Date()
}

export const mockUser2: User = {
    id: 4,
    firstName: "NameUser4",
    secondName: "SeconNameUser4",
    lastName: "LastNameUser4",
    secondLastName: "SecondLastNameUser4",
    email: "user1@email2.com",
    budget: 999999,
    createdDate: new Date(),
    updatedDate: new Date()
}

export const mockHighBudgetUser: User = {
    id: 1,
    firstName: "NameUser1",
    secondName: "SeconNameUser1",
    lastName: "LastNameUser1",
    secondLastName: "SecondLastNameUser1",
    email: "user1@email2.com",
    budget: 999999,
    createdDate: new Date(),
    updatedDate: new Date()
}

export const mockNotBudgetUser: User = {
    id: 2,
    firstName: "NameUser2",
    secondName: "SeconNameUser2",
    lastName: "LastNameUser2",
    secondLastName: "SecondLastNameUser2",
    email: "user2@email2.com",
    budget: 0,
    createdDate: new Date(),
    updatedDate: new Date()
}

export const mockUserCreateInput: UserCreateInput = {
    firstName: "NewFirstName",
    secondName: " NewSecondName",
    lastName: "NewLastName",
    secondLastName: "NewSeconLastName",
    email: "new@email.new",
    budget: 28100
}

export const mockUserUpdateInput: UserUpdateInput = {
    firstName: "UpdateFirstName",
    secondName: " UpdateSecondName",
    lastName: "UpdateLastName",
    secondLastName: "UpdateSeconLastName",
    email: "update@email.update",
    budget: 40000
}