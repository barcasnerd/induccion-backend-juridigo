import { Item, ItemCreateInput, ItemUpdateInput } from "../../../src/entities/models/item/item.model.entity";
import { mockUser } from "../data/user.mock";

export const mocksItem: Item[] = [
    {
        id: 1,
        title: "Item1",
        description: "Description for item 1",
        price: 1000,
        quantity: 31,
        createdDate: new Date(),
        updatedDate: new Date(),
        creatorUserId: mockUser.id,
    },
    {
        id: 2,
        title: "Item2",
        description: "Description for item 2",
        price: 2000,
        quantity: 13,
        createdDate: new Date(),
        updatedDate: new Date(),
        creatorUserId: mockUser.id,
    },
    {
        id: 3,
        title: "Item3",
        description: "Description for item 3",
        price: 0,
        quantity: 0,
        createdDate: new Date(),
        updatedDate: new Date(),
        creatorUserId: mockUser.id,
    }
]

export const mockItem: Item = {
    id: 1,
    title: "Item1",
    description: "Description for item 1",
    price: 1000,
    quantity: 31,
    createdDate: new Date(),
    updatedDate: new Date(),
    creatorUserId: mockUser.id,
}

export const mockItem2: Item = {
    id: 4,
    title: "Item4",
    description: "Description for item 4",
    price: 123,
    quantity: 123,
    createdDate: new Date(),
    updatedDate: new Date(),
    creatorUserId: mockUser.id,
}

export const mockNoQuantityItem: Item = {
    id: 3,
    title: "Item3",
    description: "Description for item 3",
    price: 0,
    quantity: 0,
    createdDate: new Date(),
    updatedDate: new Date(),
    creatorUserId: mockUser.id,
}

export const mockItemCreateInput: ItemCreateInput = {
    title: "New Title",
    description: "New Descrpition",
    price: 88888,
    quantity: 99999,
    creatorUserId: mockUser.id
}

export const mockItemUpdateInput: ItemCreateInput = {
    title: "Updated Title",
    description: "Updated Descrpition",
    price: 88888,
    quantity: 99999,
    creatorUserId: mockUser.id
}