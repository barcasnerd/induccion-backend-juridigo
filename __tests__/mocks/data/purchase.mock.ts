import { Purchase, PurchaseCreateInput } from "../../../src/entities/models/purchase/purchase.model.entity";
import { mockUser } from "../data/user.mock";
import { mockItem } from "../data/item.mock";

export const mocksPurchase: Purchase[] = [
    {
        id: 1,
        purchaseDate: new Date(),
        createdDate: new Date(),
        updatedDate: new Date(),
        userId: mockUser.id,
        itemsIds: [mockItem.id]
    },
    {
        id: 2,
        purchaseDate: new Date(),
        createdDate: new Date(),
        updatedDate: new Date(),
        userId: mockUser.id,
        itemsIds: [mockItem.id]
    }
]

export const mockPurchase: Purchase = {
    id: 3,
    purchaseDate: new Date(),
    createdDate: new Date(),
    updatedDate: new Date(),
    userId: mockUser.id,
    itemsIds: [mockItem.id]
}

export const mockCreatePurchaseInput: PurchaseCreateInput = {
    userId: mockUser.id,
    itemsIds: [mockItem.id]
}